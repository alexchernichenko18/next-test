import { render, screen, fireEvent, waitFor } from '@testing-library/react'
import Page from './Page'
import { createMockRouter } from 'tests/mock.ts'
import { USERS } from 'tests/mockData.ts'
import { RouterContext } from 'next/dist/shared/lib/router-context'
import axios from 'axios'
import { Provider } from 'react-redux'
import { configureStore } from '@reduxjs/toolkit'
import counterSlice from 'app/redux/reducers/counterSlice.ts'

jest.mock('axios')

const getMockReduxStore = (counterValue = 0) => {
  return configureStore({
    reducer: {
      counter: counterSlice,
    },
    preloadedState: {
      counter: {
        value: counterValue,
      },
    },
  })
}

// 1. Main Render
// 2. Renders by props
// 3. Classnames by props
// 4. Events (click)
// 5. Routes
// 6. Async
// 7. Form
// 8. State managment (Redux)

describe("Page", () => {
  // 1
  it("should has page render", () => {
    const { container } = render(
      <Provider store={getMockReduxStore()}>
        <Page />
      </Provider>
   )

    expect(screen.getByText(/Title/)).toBeInTheDocument()
    expect(container.querySelector('h2').textContent).toBe('Subtitle')
  })

  // 2
  it("should has not page render", () => {
    render(
      <Provider store={getMockReduxStore()}>
        <Page renderFlag={false} />
      </Provider>
   )

    expect(screen.queryByText(/Title/)).not.toBeInTheDocument()
  })

  // 3
  it("should has active class", () => {
    render(
      <Provider store={getMockReduxStore()}>
        <Page isActive />
      </Provider>
   )

    expect(screen.getByTestId('page')).toHaveClass('active')
  })

  // 4
  it("should open modal after button click", () => {
    const onToggleHandler = jest.fn()

    render(
      <Provider store={getMockReduxStore()}>
        <Page onToggleHandler={onToggleHandler} />
      </Provider>
   )

    const button = screen.getByText(/Modal Toggle/)
    fireEvent.click(button)

    expect(screen.getByText('Modal')).toBeInTheDocument()
    expect(onToggleHandler).toHaveBeenCalled()
  })

  // 5 For React
  // it.skip("should change route after click on link", () => {
  //   import { createMemoryHistory } from 'history'
  //   import { Router } from 'react-router-dom'

  //   const history = createMemoryHistory()

  //   render(
  //     <Router history={history} />
  //       <Provider store={getMockReduxStore()}>
  //         <Page />
  //       </Provider>
  //     </Router>
  //   )

  //   const link = screen.getByText(/Go to About page/)
  //   fireEvent.click(link)

  //   expect(router.location.pathname).toEqual('/about')
  // })

  // 5 For Next.js
  it("should change route after click on link", () => {
    const router = createMockRouter({})

    render(
      <RouterContext.Provider value={router}>
        <Provider store={getMockReduxStore()}>
          <Page />
        </Provider>
      </RouterContext.Provider>
    )
   
    const link = screen.getByText(/Go to About page/)
    fireEvent.click(link)

    expect(router.push).toHaveBeenCalled()
  })

  // 6
  it("should get users", async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: USERS }))

    render(
      <Provider store={getMockReduxStore()}>
        <Page />
      </Provider>
    )

    await waitFor(() => {
      expect(screen.queryByText(USERS[0].name)).toBeInTheDocument()
      expect(screen.getAllByTestId('user').length).toEqual(USERS.length)
    })
  })

  // 7
  it("should has correct input logic", () => {
    const testName = 'Alex'

    render(
      <Provider store={getMockReduxStore()}>
        <Page />
      </Provider>
    )

    expect(screen.queryByText(`Name: ${testName}`)).not.toBeInTheDocument()

    const input = screen.getByLabelText('name')
    fireEvent.change(input, {target: {value: testName}})

    expect(screen.getByText(`Name: ${testName}`)).toBeInTheDocument()
  })

  // 8
  it("should change redux state after click on button", async () => {
    const testCounterValue = 10
    const store = getMockReduxStore(testCounterValue)

    render(
      <Provider store={store}>
        <Page />
      </Provider>
    )

    expect(screen.getByText(testCounterValue)).toBeInTheDocument()

    fireEvent.click(screen.getByText(/Increment/))

    expect(screen.getByText(testCounterValue + 1)).toBeInTheDocument()
    expect(store.getState().counter.value).toEqual(testCounterValue + 1)

    fireEvent.click(screen.getByText(/Decrement/))
    fireEvent.click(screen.getByText(/Decrement/))

    expect(screen.getByText(testCounterValue - 1)).toBeInTheDocument()
    expect(store.getState().counter.value).toEqual(testCounterValue - 1)
  })
})
