import { useState, useEffect } from 'react'
import cn from 'classnames'
import Link from 'next/link'
import axios from 'axios'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from 'app/redux/reducers/counterSlice'

import styles from './Page.module.scss'

export default function Page ({
  renderFlag = true,
  isActive = '',
  onToggleHandler = () => {},
}) {
  const count = useSelector((state: any) => state.counter.value)
  const dispatch = useDispatch()

  const [isModalOpen, setIsModalOpen] = useState(false)
  const [users, setUsers] = useState([])
  const [name, setName] = useState('')

  const getUsers = async () => {
    const result = await axios.get('https://jsonplaceholder.typicode.com/users')
    if (result?.data) {
      setUsers(result.data)
    }
  }

  useEffect(() => {
    getUsers()
  }, [])

  const onModalToggle = () => {
    setIsModalOpen(prev => !prev)
    onToggleHandler()
  }

  if (!renderFlag) {
    return null
  }

  return (
    <div
      data-testid="page"
      className={cn(styles.wrap, isActive && styles.active)}
    >
      <h1>Title</h1>
      <h2>Subtitle</h2>
      <button onClick={onModalToggle}>Modal Toggle</button>
      {isModalOpen && (
        <div>Modal</div>
      )}
      <Link href="/about">
        Go to About page
      </Link>
      <div>
        {users?.length ? users.map((user: any, index) => {
          return (
            <div key={index} data-testid="user">
              {user.name}
            </div>
          )
        }) : null}
      </div>
      <div>
        <input
          name="name"
          aria-label="name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        {name && (
          <p>
            <span>Name: {name}</span>
          </p>
        )}
      </div>
      <div>
        <button
          aria-label="Decrement value"
          onClick={() => dispatch(decrement())}
        >
          Decrement
        </button>
        <span>{count}</span>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
        >
          Increment
        </button>
      </div>
    </div>
  )
}
