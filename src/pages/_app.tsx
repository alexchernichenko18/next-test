import Header from 'components/Header/Header'
import Footer from 'components/Footer/Footer'
import store from 'app/redux/store'
import { Provider } from 'react-redux'

import '../styles/globals.scss'

function MyApp({ Component, pageProps }: any) {
  return (
    <Provider store={store}>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </Provider>
  )
}

export default MyApp
